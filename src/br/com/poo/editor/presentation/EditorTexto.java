/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.poo.editor.presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.FileChooser;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author joao_
 */
public class EditorTexto extends JFrame implements ActionListener{
    
    JTextArea areaTexto;
    JScrollPane rolagem;
    JLabel nomeFonte;
    JSpinner mudaTamanhoFonte;
    JButton botaoCorFonte;
    JComboBox caixaFontes;
    
    JMenuBar barraMenuOpcoes;
    JMenu menuArquivos;
    JMenuItem abrirArquivo;
    JMenuItem salvaArquivo;
    JMenuItem fechaArquivo;
    
    public EditorTexto(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Editor de Texto");
        this.setSize(800,600);
        this.setLayout(new FlowLayout());
        this.setLocationRelativeTo(null);
        
        areaTexto = new JTextArea();
        areaTexto.setLineWrap(true);
        areaTexto.setWrapStyleWord(true);
        areaTexto.setFont(new Font("Arial", Font.PLAIN, 12));
        
        rolagem = new JScrollPane(areaTexto);
        rolagem.setPreferredSize(new Dimension(600,600));
        rolagem.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        
        nomeFonte = new JLabel("Fonte: ");   
        
        mudaTamanhoFonte = new JSpinner();
        mudaTamanhoFonte.setPreferredSize(new Dimension(50,25));
        mudaTamanhoFonte.setValue(12);
        mudaTamanhoFonte.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent ce) {
                areaTexto.setFont(new Font(areaTexto.getFont().getFamily(),Font.PLAIN, (int) mudaTamanhoFonte.getValue()));
            }
        });
        
        botaoCorFonte = new JButton("Cor da Fonte");
        botaoCorFonte.addActionListener(this);
        
        String[] fontes = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        
        caixaFontes = new JComboBox(fontes);
        caixaFontes.addActionListener(this);
        caixaFontes.setSelectedItem("Arial");
        
        //------Inicio: Area da barra de menu.------
        
            barraMenuOpcoes = new JMenuBar();
            menuArquivos = new JMenu("Arquivo");
            abrirArquivo = new JMenuItem("Abrir Arquivo...");
            salvaArquivo = new JMenuItem("Salvar Arquivo...");
            fechaArquivo = new JMenuItem("Fechar...");
            
            abrirArquivo.addActionListener(this);
            salvaArquivo.addActionListener(this);
            fechaArquivo.addActionListener(this);
            
            
                //------Inicio: Area da barra de menu arquivos.------
                    menuArquivos.add(abrirArquivo);
                    menuArquivos.add(salvaArquivo);
                    menuArquivos.add(fechaArquivo);
                //------Fim: Area da barra de menu arquivos.------
            barraMenuOpcoes.add(menuArquivos);
        //------Fim: Area da barra de menu.------
        
        this.setJMenuBar(barraMenuOpcoes);
        this.add(nomeFonte);
        this.add(mudaTamanhoFonte);
        this.add(botaoCorFonte);
        this.add(caixaFontes);
        this.add(rolagem);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource()==botaoCorFonte){
            JColorChooser escolheCor = new JColorChooser();
            
            Color cor = escolheCor.showDialog(null, "Escolha uma cor", Color.black);
            
            areaTexto.setForeground(cor);
        }
        
        if(ae.getSource()==caixaFontes){
            areaTexto.setFont(new Font((String)caixaFontes.getSelectedItem(),Font.PLAIN,areaTexto.getFont().getSize()));
        }
        
        if(ae.getSource()==abrirArquivo){
            JFileChooser escolhaArquivo = new JFileChooser();
            escolhaArquivo.setCurrentDirectory(new File("user.dir"));
            FileNameExtensionFilter filtro = new FileNameExtensionFilter("Arquivos de texto", "txt");
            escolhaArquivo.setFileFilter(filtro);
            
            int retorno = escolhaArquivo.showOpenDialog(null);
            
            if(retorno==JFileChooser.APPROVE_OPTION){
                File arquivo = new File(escolhaArquivo.getSelectedFile().getAbsolutePath());
                Scanner leitor = null;
                
                try {
                    leitor = new Scanner(arquivo);
                    if(arquivo.isFile()){
                        while(leitor.hasNextLine()){
                            String linha = leitor.nextLine() + "\n";
                            areaTexto.append(linha);
                        }
                    }
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(EditorTexto.class.getName()).log(Level.SEVERE, null, ex);
                }
                finally{
                    leitor.close();
                }
            }
        }
        
        if(ae.getSource()==salvaArquivo){
            JFileChooser escolhaArquivo = new JFileChooser();
            escolhaArquivo.setCurrentDirectory(new File("user.dir"));
            
            int retorno = escolhaArquivo.showSaveDialog(null);
            
            if(retorno == JFileChooser.APPROVE_OPTION){
                File arquivo;
                PrintWriter saidaArquivo = null;
                
                arquivo = new File(escolhaArquivo.getSelectedFile().getAbsolutePath());
                try {
                    saidaArquivo = new PrintWriter(arquivo);
                    saidaArquivo.println(areaTexto.getText());
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(EditorTexto.class.getName()).log(Level.SEVERE, null, ex);
                }
                finally{
                    saidaArquivo.close();
                }
            }
        }
        
        if(ae.getSource()==fechaArquivo){
            System.exit(0);
        }
    }
    
}
